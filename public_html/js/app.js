var newsletter = {};
newsletter.loaded = false;

newsletter.show = function() {
	var popup = document.getElementById("popup-guide");

	if (newsletter.loaded) {
		popup.style.opacity = '1.0';
		popup.style.visibility = 'visible';
		// localStorage.setItem("newsletter", "false");	
	} else {
		newsletter.init();
	}
};

newsletter.hide = function() {
	var popup = document.getElementById("popup-guide");
	
	if (popup) {
		popup.style.opacity = '0.0';
		popup.style.visibility = 'hidden';
		localStorage.setItem("newsletter", "true");	
	}
};

newsletter.eventHide = function(event) {
	if (this == event.target) {
		newsletter.hide();
	}
};

newsletter.setEvents = function() {
	var popup = document.getElementById("popup-guide")

	popup.addEventListener("click", newsletter.eventHide);
	document.getElementById("popup-guide-close").addEventListener("click", newsletter.eventHide);
}

newsletter.init = function() {
	var XHR = new XMLHttpRequest();

	XHR.onload = function() {
		var popup = document.getElementById("popup-guide")
		popup.innerHTML = this.responseText;
		newsletter.loaded = true;
		newsletter.setEvents();
		newsletter.show();
	};

	XHR.open("get", "/newsletter");
	XHR.send();
};

newsletter.ajaxSuccess = function() {
	document.getElementById('popup-guide').innerHTML = this.responseText;
	newsletter.setEvents();
};

newsletter.ajaxSubmit = function(formElement) {
	if (!formElement.action) { return; }

	var XHR = new XMLHttpRequest();
	XHR.onload = newsletter.ajaxSuccess;

	if (formElement.method.toLowerCase() === "post") {
		XHR.open("post", formElement.action);
		XHR.send(new FormData(formElement));
	}
};

var cookie = {};

cookie.hide = function(event) {
	document.getElementById("cookie-popup").style.bottom = "-500px";

	localStorage.setItem("cookie", "true");	
}

cookie.show = function(event) {
	document.getElementById("cookie-popup").style.bottom = "0";
	document.getElementById("cookie-popup-cross").onclick = cookie.hide;
	document.getElementById("cookie-popup-button").onclick = cookie.hide;

	localStorage.setItem("cookie", "false");	
}

window.onload = function(event) {
	cookie.status = localStorage.getItem("cookie");

	if (cookie.status != "true") {
		cookie.show();
	}

	newsletter.status = localStorage.getItem('newsletter');

	if (newsletter.status != "true") {
		newsletter.show();
	}
};
