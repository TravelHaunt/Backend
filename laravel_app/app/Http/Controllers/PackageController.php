<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\BookFormRequest;
use App\Http\Requests\QuoteFormRequest;

use DateTime;

class PackageController extends Controller
{
	public function main($id)
	{
		$data = DB::table('packages')->where('id', $id)->first();

		$date = new DateTime($data->departure_date);
		$data->departure_date = $date->format("d-m-Y");

		$date = new DateTime($data->return_date);
		$data->return_date = $date->format("d-m-Y");

		return view('package', [
			'nav_logo' => TRUE,
			'data' => $data
		]);
	}

	public function book($id)
	{
		$data = DB::table('packages')->select('name', 'departures')
			->where('id', $id)
			->first();

		$package_name = $data->name;
		$departures = explode(' - ', $data->departures);

		return view('book', [
			'nav_logo' => TRUE,
			'package_name' => $package_name,
			'departures' => $departures
		]);
	}

	public function bookPost(BookFormRequest $request, $id)
	{
		DB::table("bookings")->insert([
			"package_id" => $id,
			"departure" => $request->input('departure'),
			//"hotel" => "EMPTY", //$data["hotel"],

			"n_people" => $request->input("n_people"),
			'plus' => $request->input('plus', 'off') == 'on' ? TRUE : FALSE,
			'name' => $request->input('name'),
			"surname" => $request->input("surname"),

			//"birthday" => $request->input("birthday"), //$data["birth"],
			//"fiscalcode" => $request->input("fiscal_code"),
			"email" => $request->input("email"),
			'phone' => $request->input('phone'),
			'notes' => $request->input('notes')

			// "data" => implode(' ', $request->except([
			// 	'_token', 'departure', 'n_people', 'plus', 'name', 'surname', 'birthday', 'fiscal_code', 'email', 'notes', 'terms', 'personal'
			// 	])
			// )
		]);

		return view("success");
	}

	public function quote($id)
	{
		$data = DB::table('packages')->select('name')
			->where('id', $id)
			->first();

		return view('quote', [
			'nav_logo' => TRUE,
			'package_name' => $data->name
		]);
	}

	public function quotePost(QuoteFormRequest $request, $id)
	{
		DB::table('quotations')->insert([
			'package_id' => $id,
			"n_people" => $request->input("n_people"),
			"name" => $request->input("name"),
			"surname" => $request->input("surname"),
			"email" => $request->input("email"),
			'phone' => $request->input('phone'),
			"notes" => $request->input("notes")
		]);

		return view('success');
	}
}
