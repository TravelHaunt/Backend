<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use DateTime;

class SearchController extends Controller
{
	public function index(Request $request)
	{
		$query = DB::table('packages')->select('id', 'name', 'price', 'photo');

		if ($request->has('destination')) {
			$query->where('name', 'LIKE', $request->input('destination'));
		}

		if ($request->has('date')) {
			$date = DateTime::createFromFormat('d/m/Y', $request->input('date'));

			if ($date != false) {
				$query->whereDate('departure_date', '>=', $date->format('Y-m-d'));
			}
		}

		$packages = $query->get();

		return view('preview', [
			'nav_logo' => TRUE,
			'title' => '<span style="color: #7d0bed;">R</span>ISULTATI',
			'subtitle' => 'Ecco le nostre migliori offerte basate sulla tua ricerca.',
			'data' => $packages
		]);
	}
}
