<?php

namespace App\Http\Controllers\Categories;

use App\Http\Controllers\CategoryController;

class CheapAndFun extends CategoryController
{
	public function __construct()
	{
		$this->nav_logo = TRUE;
		$this->category_id = 1;
		$this->title = '<span style="color: #7d0bed;">CHEAP</span> n\' <span style="color: #7d0bed;">FUN</span>';
		$this->subtitle = NULL;
	}
}
