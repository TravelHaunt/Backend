<?php

namespace App\Http\Controllers\Categories;

use App\Http\Controllers\CategoryController;

class Eventi extends CategoryController
{
	public function __construct()
	{
		$this->nav_logo = TRUE;
		$this->category_id = 4;
		$this->title = '<span style="color: #7d0bed;">E</span>venti';
		$this->subtitle = NULL;
	}
}
