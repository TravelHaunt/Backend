<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');

Route::get('about', 'MainController@about');

Route::get('contact-us', 'MainController@contact');
Route::post('contact-us', 'MainController@contactPost');

Route::get("legal/privacy", "MainController@privacyPolicy");
Route::get("legal/terms-and-conditions", "MainController@termsAndConditions");

Route::get('newsletter', 'NewsletterController@index');
Route::post('newsletter', 'NewsletterController@submit');

Route::get("cheap-n-fun", "Categories\CheapAndFun@showCategoryPackages");
Route::get("around-the-world", "Categories\AroundTheWorld@showCategoryPackages");
Route::get("go-wild", "Categories\GoWild@showCategoryPackages");
Route::get("eventi", "Categories\Eventi@showCategoryPackages");

Route::get('package/{id}', 'PackageController@main');
Route::get('package/{id}/book', 'PackageController@book');
Route::post('package/{id}/book', 'PackageController@bookPost');
Route::get('package/{id}/quote', 'PackageController@quote');
Route::post('package/{id}/quote', 'PackageController@quotePost');

Route::get('search', 'SearchController@index');

