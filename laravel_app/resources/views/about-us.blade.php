@extends('layouts.main')

@section('title', 'Viaggia in Italia e nel mondo')

@section('body')
<section>
<p>
Vorremmo spiegarvi perché Travel Haunt.
</p>
<p>
Questo buffo slogan è nato per gioco: siamo amici da quando eravamo ragazzini e ci siamo affibbiati da allora la definizione di “Travel Hunters” (Cacciatori di viaggi), vista la nostra passione per questo mondo.
</p>
<p>
A 15 anni abbiamo fatto le valigie e con pochi soldi messi da parte, senza dire niente ai nostri genitori, siamo partiti per la nostra prima avventura.<br/>
Siamo sinceri… non andammo lontano, data l'età e la mancanza di denaro ci accolse Riccione, ma per noi fu un viaggio a dir poco fantasmagorico.<br/>
Negli anni successivi la nostra passione è sempre aumentata e, appena possibile, siamo partiti alla volta di nuove mete e avventure. <br/>
Nulla ha mai potuto fermarci…  lavoro, famiglia, impegni, nulla!  
<p>
Poi una sera, davanti a una birra e quasi scherzandoci sopra venne fuori un concetto che ha cambiato tutto: 
</p>
<p>
"Noi abbiamo la testa e il cuore infestati dai viaggi". 
</p>
<p>
In quel momento divenne tutto più chiaro, e quello che fino ad allora era solo un divertimento abbiamo deciso di trasformarlo nella nostra ragione di vita. 
</p>
<p>
Qual'è il nostro scopo? 
</p>
<p>
Noi siamo stati, siamo e saremo sempre dei "Cacciatori di Viaggi e Avventure", infestati da questa passione. 
</p>
<p>
Per questo <i>infesteremo</i> tutti i nostri clienti e scoveremo per loro i migliori viaggi, così da fargli vivere le emozioni più intense. 
</p>
<p>
Il sito di Travel Haunt racchiude ogni tipo di viaggio, da quelli classici a quelli più eccentrici e adrenalinici, ma sempre con il nostro tocco: quello del cacciatore <i>affamato di avventure</i>. 
</p>
<p>
Seguici, fatti infestare anche tu e vieni con noi a caccia di sogni! #hauntit
</p>
</section>
@endsection