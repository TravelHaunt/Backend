@extends('layouts.main')

@section('title', 'Viaggia in Italia e nel mondo')

@section('body')
<section class="package">
	<div class="column-1-3">
		<div class="main-photo">
			<img src="{{ url($data->photo) }}" />
		</div>

		<a class="button-buy" href="{{ Request::url() . "/book" }}">
			<div class="button">HaUNT!</div>
			<div class="price">Da {{ $data->price }}€</div>

			<div class="preventive">
				Hai bisogno di informazioni? Richiedi un preventivo senza impegno.
			</div>
		</a>
		
		<div class="reviews">
			<!--
			<div class="item">
				<div class="photo"></div>
				<div class="name">Name</div>
				<div class="content">Description</div>
			</div>

			<div class="item">
				<div class="photo"></div>
				<div class="name">Andrej Capatina</div>
				<div class="content">Hi! I'm a Baffo. I'm a nador with a stupid dickface.</div>
			</div>
			-->
		</div>
	</div>

	<div class="column-2-3 package-content">
		<div class="title">
			{{ $data->name }}
		</div>

		<div class="list">
			<ul>
				<li class="departure"><b>Partenza da</b> {{ $data->departures }}</li>
				<li class="departure-date"><b>Da</b> {{ $data->departure_date }} <b>a</b> {{ $data->return_date }} <b>(*date relative al Pacchetto PLUS+)</b></li>
				<li class="transport"><b>Mezzo</b> {{ $data->transport }}</li>
			</ul>
		</div>

		<div class="description">
			{!! $data->description !!}
		</div>


		<div class="plus">
			<h2>Pacchetto <span class="base-color">EASY</span></h2>
			<p>Parti quando vuoi!<br>
			
			<b>Trasporto</b> + <b>Hotel</b> e <b>data di partenza</b> a tua scelta.</p>
			<p>Indicaci le tue preferenze nella sezione dedicata della <b><a style="color: #7d0bed;" href="{{ Request::url() . "/book" }}">richiesta di preventivo</a></b>. 
		</div>

		<div class="plus">
			<h2>Pacchetto <span class="base-color">PLUS+</span>  ({{ $data->departure_date }} / {{ $data->return_date }})</h2>
			<p>
 				L'esperienza su misura per te.
                Fatti guidare dal nostro team e accedi ai servizi esclusivi che solo Travel Haunt ti offre!
			</p>

			<p>
				Servizi inclusi:
			</p>

			@isset($data->itinerary)
			<div class="timeline">
				{!! $data->itinerary !!}
			</div>
			@endisset

			@isset($data->extra)
			{!! $data->extra !!}
			@endisset
		</div>

		<div class="hints">
			<h2>Consigli</h2>
			<div class="intro">
				<p>
					Travel Haunt non lascia mai soli i propri viaggiatori e, al di là dell’itinerario rivelatore della personalità e della cultura della destinazione, vi offriamo una serie di consigli dove potete prendere spunto per riempire il tempo libero rimasto a disposizione.
				</p>
				<p>
					I nostri suggerimenti ripercorrono la stessa ideologia di turismo alternativo seguita nella costruzione dei nostri itinerari, avendo come ultimo fine quello di farvi cogliere le particolarità più eclatanti della vostra meta.
				</p>
			</div>

			<a class='button' href='http://blog.travelhaunt.it' target='_blank'>Scopri di più!</a>

			{{-- {!! $data->hints !!} --}}
		</div>
	</div>
</section>
@endsection