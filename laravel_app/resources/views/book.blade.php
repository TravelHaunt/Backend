@extends('layouts.main')

@section('title', 'Viaggia in Italia e nel mondo')

@section('body')
<section class="book">
	@if ($errors->any())
	@foreach ($errors->all() as $error)
	<div class="error">{{ $error }}</div> 
	@endforeach
	@endif

	<form method="post" action="{{ Request::url() }}">
		{{ csrf_field() }}

		<h2>Viaggio per {{ $package_name }}	</h2>

		<div class="block">
			<div class="title">Da dove vuoi partire?</div>
			<div class="input">
				<select name="departure">
					@foreach ($departures as $item)
					@if (old('departure') === $item)
					<option value="{{ $item }}" selected="selected">{{ $item }}</option>
					@else
					<option value="{{ $item }}">{{ $item }}</option>
					@endif
					@endforeach
				</select>
			</div>
		</div>

		<!--
		<div class="block">
			<div class="title">In che hotel vuoi alloggiare?</div>
			<div class="input">
				<select name="hotel">
					<option value="a">A</option>
					<option value="b">B</option>
					<option value="c">C</option>
					<option value="d">D</option>
				</select>
			</div>
		</div>
		-->

		<div class="block">
			<div class="title">In quanti partite?</div>
			<div class="input">
				<input type="text" name="n_people" value="{{ old('n_people') }}" />
			</div>
		</div>

		<div class="block checkbox">
			<label class="plus-cb">
				<input type="checkbox" name="plus" {{ !empty(old('plus')) ? 'checked' : '' }} />
				<span></span>
				<div class="base-color">P</div>acchetto <div class="base-color">PLUS+</div>
			</label>
		</div>

		<h2>Il referente del gruppo</h2>
		<div class="block flex">
			<div>
				<div>Nome</div>
				<input type="text" name="name" value="{{ old('name') }}" />
			</div>
			<div>
				<div>Cognome</div>
				<input type="text" name="surname" value="{{ old('surname') }}" />
			</div>
		</div>

		<!--
		<div class="block flex">
			<div>
				<div>Codice fiscale</div>
				<input type="text" name="fiscal_code" value="{{ old('fiscal_code') }}" />
			</div>
			<div>
				<div>Data di nascita</div>
				<input type="text" name="birthday" value="{{ old('birthday') }}" />
			</div>
		</div>
		-->

		<div class="block flex">
			<div>
				<div>E-mail</div>
				<input type="text" name="email" value="{{ old('email') }}" />
			</div>
			<div>
				<div>Telefono</div>
				<input type="text" name="phone" value="{{ old('phone') }}" />
			</div>
		</div>

		<!--
		<div class="block flex">
			<div>
				<div>Citta' di residenza</div>
				<input type="text" name="city" />
			</div>
			<div>
				<div>Indirizzo di resizenda</div>
				<input type="text" name="address" />
			</div>
		</div>

		<div class="block flex">
			<div>
				<div>Numero civico</div>
				<input type="text" name="civic_number" />
			</div>
			<div>
				<div>CAP</div>
				<input type="text" name="zip_code" />
			</div>
		</div>
		-->

		<!--
		<h2>Il resto del gruppo</h2>
		<div class="block flex">
			<div>
				<div>Nome</div>
				<input type="text" />
			</div>
			<div>
				<div>Cognome</div>
				<input type="text" />
			</div>
			<div>
				<div>Data di Nascita</div>
				<input type="text" />
			</div>
		</div>
		<div class="block flex">
			<div>
				<div>Nome</div>
				<input type="text" />
			</div>
			<div>
				<div>Cognome</div>
				<input type="text" />
			</div>
			<div>
				<div>Data di Nascita</div>
				<input type="text" />
			</div>
		</div>
		<div class="block flex">
			<div>
				<div>Nome</div>
				<input type="text" />
			</div>
			<div>
				<div>Cognome</div>
				<input type="text" />
			</div>
			<div>
				<div>Data di Nascita</div>
				<input type="text" />
			</div>
		</div>
		<div class="block flex">
			<div>
				<div>Nome</div>
				<input type="text" />
			</div>
			<div>
				<div>Cognome</div>
				<input type="text" />
			</div>
			<div>
				<div>Data di Nascita</div>
				<input type="text" />
			</div>
		</div>
		<div class="block flex">
			<div>
				<div>Nome</div>
				<input type="text" />
			</div>
			<div>
				<div>Cognome</div>
				<input type="text" />
			</div>
			<div>
				<div>Data di Nascita</div>
				<input type="text" />
			</div>
		</div>
		-->

		<div class="block">
			<div>Hai qualche richiesta particolare? Scrivila qui sotto</div>
			<textarea name="notes">{{ old('notes') }}</textarea>
		</div>

		<div class="block checkbox">
			<input type="checkbox" name="terms">
			<label>Dichiaro di aver letto e accettato le <a href="#">condizioni e termini d'uso</a></label>
		</div>

		<div class="block checkbox">
			<input type="checkbox" name="personal">
			<label>Autorizzo il trattamento dei miei dati personali ai sensi del d.lgs. 196 del 30 giugno 2003</label>
		</div>
		<button class="button">RICHIEDI PREVENTIVO</button>
	</form>
</section>
@endsection