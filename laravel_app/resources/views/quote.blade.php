@extends('layouts.main')

@section('title', 'Viaggia in Italia e nel mondo')

@section('body')
<section class="book">
	@if ($errors->any())
	@foreach ($errors->all() as $error)
	<div class="error">{{ $error }}</div> 
	@endforeach
	@endif

	<form method="post" action="{{ Request::url() }}">
		{{ csrf_field() }}

		<h2>Hai bisogno di informazioni riguardo un viaggio? Che cosa aspetti? Richiedi subito un preventivo per {{ $package_name }}</h2>

		<div class="block">
			<div class="title">In quanti partite?</div>
			<div class="input">
				<input type="text" name="n_people" value="{{ old('n_people') }}" />
			</div>
		</div>

		<h2></h2>
		<div class="block flex">
			<div>
				<div>Nome</div>
				<input type="text" name="name" value="{{ old('name') }}" />
			</div>
			<div>
				<div>Cognome</div>
				<input type="text" name="surname" value="{{ old('surname') }}" />
			</div>
		</div>

		<div class="block flex">
			<div>
				<div>Numero di Telefono</div>
				<input type="text" name="phone" value="{{ old('phone') }}" />
			</div>
			<div>
				<div>E-mail</div>
				<input type="text" name="email" value="{{ old('email') }}" />
			</div>
		</div>

		<div class="block">
			<div>Hai qualche richiesta particolare? Scrivila qui sotto</div>
			<textarea name="notes">{{ old('notes') }}</textarea>
		</div>

		<div class="block checkbox">
			<input type="checkbox" name="terms">
			<label>Dichiaro di aver letto e accettato le condizioni e termini d’uso</label>
		</div>

		<div class="block checkbox">
			<input type="checkbox" name="personal">
			<label>Autorizzo il trattamento dei miei dati personali ai sensi del d.lgs. 196 del 30 giugno 2003</label>
		</div>
		<button class="button">CHIEDI PREVENTIVO</button>
	</form>
</section>

@endsection