@extends('layouts.main')

@section('title', 'Viaggia in Italia e nel mondo')

@section('body')

<header>
	<div class="title">
		Go <span style="color: #7d0bed;">WILD</span>
		<div class="subtitle">Un mix di adrenalina e pura emozione.</div>
	</div>
</header>

<section class="content" id="our-offers">
	<div class="row">
		<a class="category category-small">
			<img src="images/rafting.jpg" />
			<div class="curtain">Rafting</div>
		</a>

		<a class="category category-small" href="{{ Request::url() . "/test" }}">
			<img src="images/paracadutismo.jpg" />
			<div class="curtain">Paracadutismo</div>
		</a>

		<a class="category category-small">
			<img src="images/surf.jpg" />
			<div class="curtain">Surf</div>
		</a>
	</div>

	<div class="row">
		<div class="category category-small">
			<img src="images/parapendio.jpg" />
			<div class="curtain">Parapendio</div>
		</div>

		<div class="category category-small">
			<img src="images/arrampicata.jpg" />
			<div class="curtain">Arrampicata</div>
		</div>

		<div class="category category-small">
			<img src="images/downhill.jpg" />
			<div class="curtain">Downhill</div>
		</div>
	</div>
</section>

@endsection