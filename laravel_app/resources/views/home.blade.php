@extends('layouts.main')

@section('title', 'Viaggia in Italia e nel mondo')

@section('body')
<!--
<script>
	newsletter.show();
</script>
-->

<header>
	<img src="images/logo-big.png"/>
	<div class="title">
		TRAVEL H<span style="color: #7d0bed;">a</span>UNT
		<div class="subtitle">Qualsiasi tipo di esperienza tu stia cercando, noi te la offriamo.</div>
	</div>
</header>

<section class="search">
	<form method="get" action="/search">
		<div class="block">
			<div class="title">La tua destinazione</div>
			<input type="text" placeholder="Il mondo" name="destination" />
		</div>

		<div class="block">
			<div class="title">Quando</div>
			<input type="text" placeholder="dd/mm/yyyy" name="date" />
		</div>

		<div class="block">
			<div class="title">Nr. persone</div>
			<input type="text" placeholder="Più siete, meglio è" name="n_people" />
		</div>

		<button class='button'>HaUNT!</button>
	</form>

	<div class="arrow">
		<a href="#our-offers"><img id="go-down" src="images/arrow.png"></a>
	</div>
</section>

<section class="content" id="our-offers">
	<h1>Le nostre offerte</h1>

	<div class="row">
		<a class="category" href="{{ url("cheap-n-fun") }}">
			<img src="images/cheap-n-fun.jpeg" />
			<div class="curtain">Cheap n' Fun</div>
		</a>

		<a class="category" href="{{ url("go-wild") }}">
			<img src="images/go-wild2.jpg" />
			<div class="curtain">Go Wild</div>
		</a>
	</div>

	<div class="row">
		<a class="category" href="{{ url("around-the-world") }}">
			<img src="images/around-the-world.jpg" />
			<div class="curtain">Around the world</div>
		</a>

		<a class="category" href="{{ url("eventi") }}">
			<img src="images/eventi2.jpg" />
			<div class="curtain">Eventi</div>
		</a>
	</div>
</section>
@endsection