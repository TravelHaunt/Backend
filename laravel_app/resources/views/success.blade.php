@extends('layouts.main')

@section('title', 'Viaggia in Italia e nel mondo')

@section('body')
<section class="success">
	Grazie per la prenotazione! Ti risponderemo il prima possibile entro 24 ore lavorative.
</section>
@endsection