<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>@yield('title') - Travel Haunt</title>

		<link rel="stylesheet" type="text/css" href="{{ url("/css/fonts.css") }}" />
		<link rel="stylesheet" type="text/css" href="{{ url("/css/main.css") }}" />
		<link rel="stylesheet" type="text/css" href="{{ url("/css/mobile.css") }}" />

		<script src="{{ url("/js/app.js") }}"></script>

		<!-- BEGIN: Google Analytics -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-106405736-1', 'auto');
			ga('send', 'pageview');
		</script>
		<!-- END: Google Analytics -->
		
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '352490908507905');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=352490908507905&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->
	</head>

	<body>
		<nav>
			@include('components.navigation')
		</nav>

		<div class="popup-wrapper" id="popup-guide"></div>

		<div class="cookie-popup" id="cookie-popup">
			<div class="close" id="cookie-popup-cross">X</div>
			<div class="content">
				Questo sito o gli strumenti terzi da questo utilizzati si avvalgono di cookie necessari al funzionamento ed utili alle finalità illustrate nella cookie policy. Se vuoi saperne di più o negare il consenso a tutti o ad alcuni cookie, consulta la <a href="#">cookie policy</a>.<br/>
				<br />
				Chiudendo questo banner, scorrendo questa pagina, cliccando su un link o proseguendo la navigazione in altra maniera, acconsenti all'uso dei cookie.
			</div>
			<div class="button" id="cookie-popup-button">OK!</div>
		</div>

		<div class="wrapper">
			@section('body')
				Body
			@show
		</div>

		<footer class="main">
			@include('components.footer')
		</footer>
	</body>
</html>