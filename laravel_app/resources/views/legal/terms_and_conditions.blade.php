@extends('layouts.main')

@section('title', 'Viaggia in Italia e nel mondo')

@section('body')
<header>
	<div class="title">
		<span style="color: #7d0bed;">T</span>ermini e condizioni
		<div class="subtitle"></div>
	</div>
</header>

<section>
		<p>Ultimo aggiornamento: 01 luglio 2017</p>

		<p>Leggi questi Termini e condizioni ("Termini", "Termini e condizioni")
		Attentamente prima di utilizzare il sito web www.travelhaunt.it (il "Servizio")
		Da Travel Haunt ("noi", "voi" o "il nostro").</p>

		<p>L'accesso e l'utilizzo del Servizio Ë condizionato all'accettazione e
		conformitá a questi Termini. Questi termini si applicano a tutti i visitatori, gli utenti e
		altri che accedono o utilizzano il Servizio.</p>

		<p>Accedendo o utilizzando il Servizio acconsenti ad essere vincolato da queste Condizioni. Se sei
		in disaccordo con qualsiasi parte dei termini, allora non potrai accedere al Servizio. Questi
		Termini e condizioni contrattuali sono concessi in licenza da [TermsFeed] (https://termsfeed.com)
		a Travel Haunt.</p>
		

		<h2>Accounts</h2>

		<p>Quando crei un account con noi, devi fornire informazioni accurate, complete e aggiornate in ogni momento. 
		La mancanza di questi requisito costituisce una
		Violazione dei Termini, che puó  comportare la cancellazione immediata del tuo account
		dal nostro Servizio.</p>

		<p>Tu sei responsabile della salvaguardia della password utilizzata per accedere al
		Servizio e per tutte le attivitá o azioni sotto la tua password, sia che utilizzi la tua
		password per il nostro Servizio o per un servizio di terze parti.</p>

		<p>L'utente accetta di non divulgare la propria password a terzi. Devi comunicarci
		Immediatamente se sei a conoscenza di qualsiasi violazione della sicurezza o dell'uso non autorizzato
		Del tuo account.</p>


		<h2>Collegamenti ad altri siti web</h2>

		<p>Il nostro Servizio può contenere collegamenti a siti o servizi di terze parti che non sono
		di proprietà o controllati da Travel Haunt.</p>

		<p>Travel Haunt non ha alcun controllo e non si assume alcuna responsabilità sui
		Contenuti, norme sulla privacy o pratiche di qualsiasi sito web di terze parti o
		Servizi. Inoltre riconosci e accetti che Travel Haunt non sarà
		Responsabile, direttamente o indirettamente, per eventuali danni o perdite causate
		o che si supponga essere stati causati da o in connessione con l'uso o in relazione di
		tali contenuti, beni o servizi disponibili su o attraverso tali siti web oppure
		Servizi.</p>

		<p>Ti consigliamo vivamente di leggere i termini e le condizioni e le norme sulla privacy
		di qualsiasi sito web o servizi di terze parti che si visita.</p>


		<h2>Risoluzione</h2>

		<p>Possiamo interrompere o sospendere immediatamente l'accesso al nostro Servizio, senza alcun preavviso
		Preavviso o responsabilità, per qualsiasi motivo, compreso la cessazione senza limitazione
		se violi i Termini.</p>

		<p>Tutte le disposizioni dei Termini che per loro natura devono sopravvivere alla risoluzione
		e sopravvivranno alla risoluzione, inclusa, senza limitazione, la proprietà,
		disposizioni, garanzie di disconoscimento, indennità e limitazioni di responsabilità.</p>

		<p>Possiamo eliminare o sospendere il tuo account immediatamente, senza preavviso o
		Responsabilità, per qualsiasi motivo, compreso senza limitazione se
		Violi i Termini.</p>

		<p>Con l'eliminazione, il tuo diritto di utilizzare il Servizio cesserà immediatamente. Se 
		desideri eliminare il tuo account, puoi semplicemente interrompere l'utilizzo del Servizio.</p>

		<p>Tutte le disposizioni dei Termini che per loro natura devono sopravvivere alla risoluzione
		sopravvivranno alla risoluzione, inclusa, senza limitazione, la proprietà,
		disposizioni, garanzie di disconoscimento, indennità e limitazioni di responsabilità.</p>


		<h2>Legge governativa</h2>

		<p>Questi termini saranno disciplinati e interpretati in conformità alle leggi dell'Italia,
		senza tener conto delle disposizioni in materia di conflitto di legge.</p>

		<p>La nostra mancata applicazione di qualsiasi diritto o disposizione di questi termini non sarà
		considerato una rinuncia a tali diritti. Se si ritiene che una qualsiasi delle presenti Condizioni
		sia non valida o inapplicabile da un tribunale, le restanti disposizioni di questi
		termini rimarranno in vigore. Questi termini costituiscono l'intero accordo
		Tra noi per quanto riguarda il nostro Servizio, in sostituzione di qualsiasi accordi precedenti
		che potrebbero esserci stati fra noi per quanto riguarda il Servizio.</p>


		<h2>Sicurezza</h2>

		<p>Ci riserviamo il diritto, a nostra esclusiva discrezione, di modificare o sostituire questi Termini
		in ogni momento. Se la revisione è rilevante cercheremo di fornire almeno 30 giorni di
		previa notifica di nuovi termini. Che cosa costituisce un cambiamento rilevante
		sará determinato a nostra esclusiva discrezione.</p>

		<p>Continuando ad accedere o utilizzare il nostro servizio dopo che tali revisioni diventano
		efficaci, accetti di essere vincolato dai termini modificati. Se non sei d'accordo
		sui nuovi termini, si prega di smettere di utilizzare il Servizio.</p>


		<h2>Contattaci</h2>

		<p>Se hai domande su questi termini, contattaci.</p>
	</section>
@endsection