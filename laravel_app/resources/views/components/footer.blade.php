<div class="flex-wrapper wrapper">
	<img src="{{ url("images/logo-big.png") }}" />
	<div class="contacts">
		<p>
			<b>Per info, prenotazioni o preventivi, contatta</b>
			<div class="phone">Gabriele: +39 331 901 3911</div>
			<div class="phone">Olga: +39 345 695 1684</div>
			<br />
			Siamo anche disponibili per messaggi su WhatsApp.
		</p>

		<p>
			<b>Oppure manda una mail</b>
			<div class="mail">
				info@travelhaunt.it
			</div>
		</p>
	</div>

	<div class="legal">
		<div class="iva">
			Powered by Acacia Travel soc. coop. A.r.l<br />
			N. REA: N65475, Italy License N°229/S.9/Tur<br />
			Via della Repubblica 8, Santa Croce sull'Arno (PI) Tuscany, Italy.<br/>
			P. IVA/V.A.T. 01173420868<br />
			Iscrizione alla camera di commercio: PI193758
		</div>
	
		<div class="copyright">
			<div class="element">© Icon made by Freepik from www.flaticon.com</div>
			<div class="element">© Photos by Fotolia.com</div>
			<div class="element">© Photos by Shutterstock.com</div>
		</div>
	</div>
</div>

<div class="others">
	<a href="{{ url("/legal/privacy") }}">Privacy</a>
	<a href="{{ url("/legal/terms-and-conditions") }}">Termini & Condizioni</a>
</div>