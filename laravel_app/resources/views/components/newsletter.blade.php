<div class="popup">
	<div class="close" id="popup-guide-close">X</div>

	<div id="popup-guide-content">
		<div class="title">Ciao!</div>
		<div class="content">
			Iscriviti e scarica <b>GRATIS</b> la nuova guida alle<br />
			<span style="color: #7d0bed; font-weight: bold;">"10 città imperdibili per il 2017" </span>
		</div>

		@if ($errors->any())
		@foreach ($errors->all() as $error)
		<div class="error">{{ $error }}</div> 
		@endforeach
		@endif
		
		<form method="post" action="/newsletter" onsubmit="newsletter.ajaxSubmit(this); return false;">
			{!! csrf_field() !!}
			<input type="text" placeholder="Nome" name="name" value="{{ old('name') }}" />
			<input type="text" placeholder="peter.parker@mail.com" name="email" value="{{ old('email') }}" />
			<button class="button">ISCRIVITI!</button>
		</form>
	</div>
</form>